<?php
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Avanti_RemoveTaxCartLabel',
    __DIR__
);

