<?php
declare(strict_types=1);

namespace Avanti\RemoveTaxCartLabel\Plugin\Magento\Checkout\Block\Cart;

use Magento\Checkout\Block\Cart\CartTotalsProcessor as CartTotalsProcessorCore;

class CartTotalsProcessor
{
    public function afterProcess(
        CartTotalsProcessorCore $subject,
        $result
    ) {
        if (isset($result['components']['block-totals']['children']['tax'])) {
            unset($result['components']['block-totals']['children']['tax']);
        }
        return $result;
    }
}

