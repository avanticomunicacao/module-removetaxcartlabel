<?php
declare(strict_types=1);

namespace Avanti\RemoveTaxCartLabel\Plugin\Magento\Checkout\Block\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor as LayoutProcessorCore;

class LayoutProcessor
{
    public function afterProcess(LayoutProcessorCore $subject, $result)
    {
        if (isset($result["components"]["checkout"]["children"]["sidebar"]["children"]["summary"]["children"]["totals"]["children"]["tax"])) {
            unset($result["components"]["checkout"]["children"]["sidebar"]["children"]["summary"]["children"]["totals"]["children"]["tax"]);
        }
        return $result;
    }
}
